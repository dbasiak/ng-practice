import { Component, OnInit } from '@angular/core';

interface IHeader {
  title: string;
  subtitle: string;
  description: string;
}
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  header: IHeader = {
    title: 'Master branch',
    subtitle: 'mb',
    description: 'test'
  }
  constructor() { }

  ngOnInit() {
  }

}
